import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ServiciosService } from '../servicios.service';
import { OwlOptions } from 'ngx-owl-carousel-o';
import {first} from "rxjs/operators";

import { NgxUiLoaderService } from 'ngx-ui-loader'; // Import NgxUiLoaderService
@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
})
export class LandingComponent implements OnInit {    
  width;
  validateForm: FormGroup;
  submitted = false;
  customOptions: OwlOptions;
  arrayExpedicion = [
    {
      value: 'SC',
      label: 'Santa Cruz'
    },
    {
      value: 'LP',
      label: 'La Paz'
    },
    {
      value: 'CB',
      label: 'Cochabamba'
    },
    {
      value: 'OR',
      label: 'Oruro'
    },
    {
      value: 'TJ ',
      label: 'Tarija'
    },
    {
      value: 'BE',
      label: 'Beni'
    },
    {
      value: 'CH',
      label: 'Chuquisaca'
    },
    {
      value: 'PO',
      label: 'Potosi'
    },
    {
      value: 'PA',
      label: 'Pando'
    },
    {
      value: 'PE',
      label: 'Persona Extranjera'
    }
  ];

  arrayMoneda = [];
  arrayDias = [];
  arrayMes = [];
  arrayAnio = [];
  producto;
  imagenProducto;

  terminos: boolean;
  datosCorreo:FormData;
  constructor(private formBuilder: FormBuilder, private servicio: ServiciosService, private ngxService: NgxUiLoaderService) {
    this.resize(); // Llamado a la función resize

    // Generamos el array de días
    for (let i = 1; i <=31 ; i++ ) {
      this.arrayDias.push(i);      
    }

    // Generamos el array de meses
    for (let i = 1; i <=12 ; i++ ) {
      this.arrayMes.push(i);      
    }    

    // Generamos el array de años
    const fecha = new Date();
    const ano = fecha.getFullYear();
    for (let i = (ano - 100); i <= (ano-18) ; i++ ) {
      this.arrayAnio.push(i);      
    }
  }
  // Función para el efecto hover de las cajas
  toggleClassProductos(e) {
    const elemento = e.target;
    const elementoPadre = elemento.parentNode;
    elemento.classList.forEach((index) => {      
      if (index === 'cont_carru' && index !== 'active') {        
        elemento.classList.add('active');
      } else {
        elementoPadre.classList.forEach(clasesPadre => {
          if (clasesPadre === 'cont_carru' && clasesPadre !== 'active') {
            elementoPadre.classList.add('active');
          }
        });
      }
    })
    
  }

  // Función que detecta el evento resize
  resize() {
      this.width = innerWidth;    
  }

  quitarClaseActiva(e) {
      const elementos = document.querySelectorAll('.cont_carru');
      elementos.forEach(index => {      
        index.classList.remove('active');      
      })
  }

  mostrarDireccion(selectObject) {
      const selected_sucursal = selectObject.target.value;

      if (selected_sucursal == '') {
          document.querySelector('#modal-formulario .direcciones-fsucursal').innerHTML = "";
      }

      if (selected_sucursal == 'blacutt') {
          document.querySelector('#modal-formulario .direcciones-fsucursal').innerHTML = "Av. Velarde Esq. Andrés Manso Nº 200.";
      }
      
      if (selected_sucursal == 'norte') {
          document.querySelector('#modal-formulario .direcciones-fsucursal').innerHTML = "Sobre Av. Cristo Redentor, entre 3er y 4to. Anillo.";
      }
      
  } 

  mostrarForm(img, p) {
      this.imagenProducto = img;
      this.producto = p;
      // Combo Gana doble/ Gana mas
      if (p === 1 && img === 1) {
        this.arrayMoneda = [
          {
            value: 3331,
            label: 'Bs/$us'
          }
        ];
      }
      // Gana Mas
      if (p === 1 && img === 2) {
        this.arrayMoneda = [
          {
            value: 0,
            label: 'Bs'
          },
          {
            value: 2225,
            label: '$us'
          },
          {
            value: 1112,
            label: 'Euros'
          },
          {
            value: 3331,
            label: 'Bs/$us'
          }

          
        ];
      }
      // Gana Doble
      if (p === 0 && img === 3) {
        this.arrayMoneda = [
          {
            value: 0,
            label: 'Bs'
          }
        ];
      }

      document.querySelector('#modal-formulario').classList.add('sticky_formulario');
  }

  ocultarForm () {
      document.querySelector('#modal-formulario').classList.remove('sticky_formulario');
  }

  mostrarModalRespuesta() {
    //Mostrar modal respuesta
    var modal_respuesta = document.querySelector("#modal-respuesta");
    modal_respuesta.classList.add("sticky_respuesta"); 

    var transparencia_modal_respuesta = document.querySelector("#transparencia-modal-respuesta");
    transparencia_modal_respuesta.classList.add("sticky_respuesta"); 
  }

  cerrarModalRespuesta() {
    //Mostrar modal respuesta
    var modal_respuesta = document.querySelector("#modal-respuesta");
    modal_respuesta.classList.remove("sticky_respuesta"); 

    var transparencia_modal_respuesta = document.querySelector("#transparencia-modal-respuesta");
    transparencia_modal_respuesta.classList.remove("sticky_respuesta"); 
  }

  mostrarModalSeguro(){
    var modal = document.querySelector("#modal-seguro");
    modal.classList.add("active");    
  }

  cerrarModalSeguro(){
    var modal = document.querySelector("#modal-seguro");
    modal.classList.remove("active");    
  }

  mostrarModalSegip(){
    var modal = document.querySelector("#modal-segip");
    modal.classList.add("active");    
  }

  cerrarModalSegip(){
    var modal = document.querySelector("#modal-segip");
    modal.classList.remove("active");    
  }

  onClickedOutside(e) {
    
    
  }

  ngOnInit() {
  		this.datosCorreo = new FormData();
      this.validateForm = this.formBuilder.group({
        ftelefono: ['', Validators.required],
        fdocumento: ['', Validators.required],      
        fexp: ['', Validators.required],
        fcomplemento: [''],
        fdiaNac: ['', Validators.required],
        fmesNac: ['', Validators.required],
        fanioNac: ['', Validators.required],
        fciudadano: [false],
        fterminos: ['', Validators.required],
        fgenero: ['Masculino'],
        fingreso: ['', Validators.required],
        fnit: [''],
        femail: ['', [Validators.email, Validators.required]],
        fmonedaCuenta: ['', Validators.required],
        fseguro: [true],
        fsucursal: ['', Validators.required]
      });

      var margin_carru =34;
      var loop_carru = false;
      var autoplay_carru = false;
      var center_carru = false

      if (this.width <= 992) {
          margin_carru = 10;
      }
      if (this.width <= 768) {
          margin_carru = 50;
          loop_carru = true;
          autoplay_carru = false;
          center_carru = true;
      }

      this.customOptions = {
        center: center_carru,
        margin: margin_carru,
        nav: false,
        dots:false,
        freeDrag: true,
        loop: loop_carru,
        items: 2,
        responsive: {  0:{items: 1.6}, 
                        576: {items: 2}, 
                        768: {items: 3}, 992: {items: 3}, 
                        1200: {items: 3}, 
                        1600: {items: 3}  }, //xs, sm, ...
        autoplay: autoplay_carru,
        autoplayTimeout: 4500,
        autoplayHoverPause: true,      
        smartSpeed:450
      }

      var input = document.querySelectorAll(".inputclass_animated");
      Array.prototype.forEach.call(input, function(elements, index) {
              elements.onfocus = function() {
                      var lavel_hover = this.previousElementSibling;
                          lavel_hover.classList.add("lplace_holder_move", "active_l");
              };
              elements.onblur = function() {
                      if (this.value == "") { //si el input esta vacia 
                          var lavel_hover = this.previousElementSibling;
                          lavel_hover.classList.remove("active_l");
                      }                    
              };
      });
  }

  get f() { return this.validateForm.controls; }

  submitForm() {
      this.submitted = true;
      this.ngxService.start();  
      for (const i in this.validateForm.controls) {
        this.validateForm.controls[i].markAsDirty();
        this.validateForm.controls[i].updateValueAndValidity();
      }

      // stop here if form is invalid
      if (this.validateForm.invalid) {
          this.ngxService.stop();
          return;
      }

      const datosPaso1 = {
        pNroTelefono: parseInt(this.f.ftelefono.value, 10),
        pCodigoSms: 0,
        pCanal: "1",
        pIp: "0.0.0.0",
        pIdDispositivo: "web",
        pKsbg: "0",
        pModelo: "0",
        pSOperativo: "0",
        pProducto: this.producto.toString()
      }

      this.servicio.paso1(datosPaso1).pipe(first()).subscribe(
      	respPaso1 => {
					// converting to DOM Tree
          const parser = new DOMParser();
          const srcDOMPaso1 = parser.parseFromString(respPaso1, "application/xml");

          // Converting DOM Tree To JSON. 
          const xmlJsonPaso1 = this.xml2json(srcDOMPaso1);
          console.log(xmlJsonPaso1);
          const errorPaso1 = xmlJsonPaso1['mtdServRegistrarACPaso1_KioscoResponse']['mtdServRegistrarACPaso1_KioscoResult']['a:intCodError'];
          const nroSolicitud = xmlJsonPaso1['mtdServRegistrarACPaso1_KioscoResponse']['mtdServRegistrarACPaso1_KioscoResult']['a:intNroSolicitud'];
          if (errorPaso1 === '0' ) {
          	 const datosPaso2 = {
				        pNroSolicitud: nroSolicitud,
				        pNroDocumento: this.f.fdocumento.value,
				        pExpedicion: this.f.fexp.value,
				        pComplemento: this.f.fcomplemento.value,
				        pFechaNacimiento: `${this.f.fdiaNac.value}/${this.f.fmesNac.value}/${this.f.fanioNac.value}`,
				        fciudadano: this.f.fciudadano.value,
				        pSexo: this.f.fgenero.value === 'Masculino' ? 'M': 'F',
				        pEstadoCivil: 'S',
				        pUsaApCasada: 'N',
				        pGreenCard: this.f.fciudadano.value ? 'S' : 'N',
				        pNivelIngreso: this.f.fingreso.value,
				        pNit: this.f.fnit.value,
				        pEmail: this.f.femail.value,
				        pTarjeta: 'S',
				        pSeguro: this.f.fseguro.value ? 'S' : 'N',
				        pMoneda: this.f.fmonedaCuenta.value
				      }

				      this.servicio.paso2(datosPaso2).pipe(first()).subscribe(
				      	respPaso2 => {
									const srcDOMPaso2 = parser.parseFromString(respPaso2, "application/xml");

				          // Converting DOM Tree To JSON. 
				          const xmlJsonPaso2 = this.xml2json(srcDOMPaso2);
				          console.log(xmlJsonPaso2);
				          const errorPaso2 = xmlJsonPaso2['mtdServRegistraraACPaso2Response']['mtdServRegistraraACPaso2Result']['a:intCodError'];

				          if(errorPaso2 === '0') {
				          	const datosPaso3 = {
							        pNroSolicitud: nroSolicitud,
							        pNPS: 10,
							        pTextoNPS: "Envio desde la landing Page",
							      }

							      this.servicio.paso3(datosPaso3).pipe(first()).subscribe(
							      	respPaso3 => {
												const srcDOMPaso3 = parser.parseFromString(respPaso3, "application/xml");

							          // Converting DOM Tree To JSON. 
							          const xmlJsonPaso3 = this.xml2json(srcDOMPaso3);
							          console.log(xmlJsonPaso3);
							          const errorPaso3 = xmlJsonPaso3['mtdServRegistrarACNpsResponse']['mtdServRegistrarACNpsResult']['a:intCodError'];

							          if (errorPaso3 === '0') {
							          	
							          	this.datosCorreo.append('email', this.f.femail.value);
							          	this.datosCorreo.append('sucursal', this.f.fsucursal.value);
                          this.datosCorreo.append('producto', '1');
                          
                          // Para envio de correo con servicio en net core descomentar toda la constante y quitar  this.datosCorreo, al servicio se envia la constante
							          	/* const datosCorreo = {
										        email: this.f.femail.value,
										        sucursal: this.f.fsucursal.value,
										        producto: '1'
										      }*/ 
										      this.servicio.correo(this.datosCorreo).pipe(first()).subscribe(
											        data => {          
											          if (data.error === 0) {
                                    this.ngxService.stop();
											              this.mostrarModalRespuesta();
											              // REDIRECCIONAR web del banco LUEGO mostrar modal 12 Seg
											                
											                setTimeout(function(){
											                    window.open("https://www.bg.com.bo/")
											                }, 12000);
											                
											          }
											        }, error1 => {
                                console.log(error1);
                                this.ngxService.stop();
											        }
											      );
							          } else {
                          console.log(xmlJsonPaso3);
                          this.ngxService.stop();
                        }
							      	}
						      	);
				          } else {
                    this.ngxService.stop();
                    console.log(xmlJsonPaso2);
                  }
				      	}
			      	);

          } else {
            console.log(xmlJsonPaso1);
            this.ngxService.stop();
          }
			    
      	}, error => {
          console.log(error);
          this.ngxService.stop();
      	}
    	);
  }

  xml2json(srcDOM) {
    let children = [...srcDOM.children];
  
    // base case for recursion. 
    if (!children.length) {
      return srcDOM.innerHTML
    }
  
    // initializing object to be returned. 
    let jsonResult = {};
  
    for (let child of children) {
  
      // checking is child has siblings of same name. 
      let childIsArray = children.filter(eachChild => eachChild.nodeName === child.nodeName).length > 1;
  
      // if child is array, save the values as array, else as strings. 
      if (childIsArray) {
        if (jsonResult[child.nodeName] === undefined) {
          jsonResult[child.nodeName] = [this.xml2json(child)];
        } else {
          jsonResult[child.nodeName].push(this.xml2json(child));
        }
      } else {
        jsonResult[child.nodeName] = this.xml2json(child);
      }
    }
  
    return jsonResult;
  }

}
