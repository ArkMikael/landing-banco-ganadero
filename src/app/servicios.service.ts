import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ServiciosService {
public xmlItems: any;
  constructor(private http: HttpClient) { }

  // Servicio Paso 1
  paso1 (datos: any) {
    const url = `WsAperturaCuenta/Service1.svc/mtdServRegistrarACPaso1_Kiosco?pNroTelefono=${datos.pNroTelefono}&pCodigoSms=${datos.pCodigoSms}&pCanal=${datos.pCanal}&pIp=${datos.pIp}&pIdDispositivo=${datos.pIdDispositivo}&pKsbg=${datos.pKsbg}&pModelo=${datos.pModelo}&pSOperativo=${datos.pSOperativo}&pProducto=${datos.pProducto}`;    
    return this.http.get(url, { observe: 'body', responseType: 'text'});
  }

  // Servicio Paso 2
  paso2(data) {
    const url = `WsAperturaCuenta/Service1.svc/mtdServRegistrarACPaso2?pNroSolicitud=${data.pNroSolicitud}&pNroDocumento=${data.pNroDocumento}&pComplemento=${data.pComplemento}&pExpedicion=${data.pExpedicion}&pFechaNacimiento=${data.pFechaNacimiento}&pSexo=${data.pSexo}&pEstadoCivil=${data.pEstadoCivil}&pUsaApCasada=${data.pUsaApCasada}&pGreenCard=${data.pGreenCard}&pNivelIngreso=${data.pNivelIngreso}&pNit=${data.pNit}&pEmail=${data.pEmail}&pTarjeta=${data.pTarjeta}&pSeguro=${data.pSeguro}&pMoneda=${data.pMoneda}`;
    return this.http.get(url, { observe: 'body', responseType: 'text'});
  }

  // Servicio Paso 3
  paso3(data) {
    const url = `WsAperturaCuenta/Service1.svc/mtdServRegistrarACNps?pNroSolicitud=${data.pNroSolicitud}&pNPS=${data.pNPS}&pTextoNPS=${data.pTextoNPS}`;    
    return this.http.get(url, { observe: 'body', responseType: 'text'});
  }

  // Servicio Paso 3
  correo(data) {    
    const url = `http://bganadero.test-battle.com/components/Post/email_post.php`;    
    return this.http.post<any>(url, data);

    // Para servicio con net core descomentar ambas lineas y cambiar la ip por la ruta del servidor . PD: se debe comentar ambas lineas de arriba
    // const url = `http://192.168.1.8:5000/api/correo/enviar/email=${data.email}&sucursal=${data.sucursal}&producto=0`;
    // return this.http.get<any>(url, httpOptions);
  }

}
